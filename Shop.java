import java.util.Scanner;
public class Shop
{

 public static void main(String[] args)
 {
   Scanner scan = new Scanner(System.in);
   
  Game[] games = new Game[4];
  
  //populate array
  for(int i = 0; i < games.length; i++)
  {
   games[i] = new Game();
   
   System.out.println("Please enter the title for game #" + (i + 1));
   games[i].title = scan.next();
   System.out.println("Please enter the publisher for game #" + (i + 1));
   games[i].publisher = scan.next();
   System.out.println("Please enter the genre for game #" + (i + 1));
   games[i].genre = scan.next();
   System.out.println("Please enter the price for game #" + (i + 1));
   games[i].price = scan.nextDouble();
   System.out.println("Please enter the discount in % for game #" + (i + 1));
   games[i].discountPercent = scan.nextDouble();
   
  }
  
  System.out.println("For the last product, the title is: " + games[3].title);
  System.out.println("The publisher is " + games[3].publisher);
  System.out.println("The price is " + games[3].price);
  System.out.println("The discount in percentage is " + games[3].discountPercent);
  System.out.println("The genre is " + games[3].genre);
  
  games[3].PrintDiscountedPrice();
  
 }
}