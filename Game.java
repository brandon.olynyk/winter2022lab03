public class Game
{
 public String title;
 public String publisher;
 public double price;
 public double discountPercent;
 public String genre;
 
 public void PrintDiscountedPrice()
 {
  double discountedPrice = price * (discountPercent / 100);
  System.out.print("The discounted price for " + title + " is: " + (price - discountedPrice));
 }
}